import qualified Data.ByteString.Char8 as BS
import qualified Data.Text.Encoding as T
import qualified Data.Text.IO as T

main :: IO ()
main = do
	file <- T.readFome "text4.hs"
	let str = T.encodeUtf8 file
	BS.putStrLn str