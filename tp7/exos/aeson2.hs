{-# LANGUAGE OverloadedStrings #-}
import qualified Data.Aeson
import qualified Data.Text as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Show)


instance A.FromJSON Person where
    parseJSON = withObject "Person" $ \v -> do
        f <- v .: "firstname"
        l <- v .: "lastname"
        b <- v .: "birthyear"
        s <- v .: "speakenglish" return $ Person f l b s

main :: IO ()
main = do
    res1 <- eitherDecodeFileStrict' "aeson-test1.json"
    print (res1 :: either String Person)
    res2 <- eitherDecodeFileStrict' "aeson-test2.json"
    print (res2 :: either String Person)
    res3 <- eitherDecodeFileStrict' "aeson-test3.json"
    print (res3 :: either String Person)