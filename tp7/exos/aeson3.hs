{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T

data Person = Person
    { first    :: T.Text
    , last     :: T.Text
    , birth    :: Int
    } deriving (Show)

instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> do
        f <- v .: "firstname"
        l <- v .: "lastname"
        b <- v .: "birthyear"
        return $ Person f l (read b)

main :: IO ()
main = do
    let res0 = Person "John" "Doe" 1970
    print res0

