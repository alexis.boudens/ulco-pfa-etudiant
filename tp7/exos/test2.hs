import qualified Data.ByteString.Char8 as BS
import qualified Data.Text.Encoding as T

main :: IO()
main = do
    file <- BS.readFile "text2.hs"
    let str = T.decodeUtf8 file
    print str