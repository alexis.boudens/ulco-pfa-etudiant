import qualified Data.Text.IO as T
import qualified Data.Text.Lazy as TL

main :: IO()
main = do
    file <- T.readFile "text5.hs"
    let str = TL.fromStrict file
    print str