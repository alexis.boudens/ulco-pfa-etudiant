import Data.List

kernel :: Numa a => a -> a
kernel = (*2) . (+1)

compute :: Numa [a] => [a] -> a
compute = foldl' (\ acc x -> acc + kernel x) 0

main = do
    print $ compute [1..3::Int]
    print $ compute [1..3::Double]

