data Tree a  = Leaf | Node a (Tree a) (Tree a)

mytree1 :: Num a => Tree a
mytree1 = Node 7 (Node 2 Leaf Leaf)
                (Node 37 (Node 13 Leaf Leaf)
                         (Node 42 Leaf Leaf))

instance Show a => Show (Tree a) where
    show Leaf = "_"
    show (Node e lt rt) = "(" ++ show e ++ show lt ++ show rt ++ ")"

main = do
    print mytree1
    -- print mytree2
    -- print $ sum mytree1
    -- print $ maximum mytree1

