-- threshList
threshList :: Ord a => a -> [a] -> [a]
threshList mx = map (min mx)

-- selectList
selectList :: (Ord a) => a -> [a] -> [a]
selectList select arr = filter(<select) arr

-- maxList
maxList :: Ord a => [a] -> a
maxList [] = error "Empty list"
maxList (h:t) = foldl (\x element -> (max x element)) h t

main :: IO ()
main = do
    print $ maxList [1..3]
