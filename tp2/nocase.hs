-- isSorted
isSorted :: (Ord a) => [a] -> Bool
isSorted [] = True
isSorted [_] = True
isSorted (x:y:xs) = x <= y && isSorted (y:xs)

-- nocaseCmp 
-- nocaseCmp :: String -> String -> Bool
-- nocaseCmp a b = 
--     let 
--         lowerA = (map toLower a)
--         lowerB = (map toLower b)
--     in lowerA < lowerB

main :: IO ()
main = do
    print $ isSorted [1, 4, 2, 6]

