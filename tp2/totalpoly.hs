
-- safeTailString 
safeTailString :: String -> String
safeTailString "" = ""
safeTailString (_:t) = t

-- safeHeadString 

-- safeTail 
safeTail :: [a] -> [a]
safeTail [] = []
safeTail (_: xs) = xs

-- safeHead
safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x: _) = Just x


main :: IO ()
main = do
    print $ safeTail [1,2]
    print $ safeHead ""
    print $ safeHead "test"

