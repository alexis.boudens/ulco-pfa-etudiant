
-- mymap1 
mymap1 :: (a -> a) -> [a] -> [a]
mymap1 _ [] = []
mymap1 fn (h:t) = (fn h) : (mymap1 fn t)

-- mymap2
mymap2 :: (a -> a) -> [a] -> [a]
mymap2 fn list = 
    mymap2_int fn list []
    where
        mymap2_int :: (a -> a) -> [a] -> [a] -> [a]
        mymap2_int _ [] acc = acc 
        mymap2_int fn (h:t) acc = mymap2_int fn t (acc ++ [(fn h)])

-- myfilter1 
myfilter1 :: (a -> Bool) -> [a] -> [a]
myfilter1 _ [] = []
myfilter1 f (h:t)
    | f h = (h:myfilter1 f t)
    | otherwise = myfilter1 f t

-- myfilter2 
myfilter2 :: (a -> Bool) -> [a] -> [a]
myfilter2 p xs = go xs []
    where go [] acc = acc
          go (y: ys) acc = go ys (if p y then acc ++ [y] else acc)

-- myfoldl 

-- myfoldr 

main :: IO ()
main = do
    print $ mymap2 (*2) [1..4]

