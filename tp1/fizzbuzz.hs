fizzbuzz :: Int -> String
fizzbuzz x
    | x `mod` 15 == 0 = "fizzbuzz"
    | x `mod` 5 == 0 = "buzz"
    | x `mod` 3 == 0 = "fizz"
    | otherwise = show x

fizzbuzz1 :: [Int] -> [String]
fizzbuzz1 [] = []
fizzbuzz1 (x: others) = fizzbuzz x : fizzbuzz1 others

fizzbuzz2 :: [Int] -> [String]
fizzbuzz2 xs = reverse $ aux xs []
    where aux [] acc = acc
          aux (x: others) acc = aux others (fizzbuzz x: acc)

main :: IO ()
main = do
    print $ fizzbuzz1 [1..15]
    print $ fizzbuzz2 [1..15]

