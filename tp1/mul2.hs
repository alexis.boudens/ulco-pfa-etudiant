mul2 :: Int -> Int
mul2 = (*2)

main :: IO ()
main = do
    print $ mul2 5
    print $ mul2 (-42)

