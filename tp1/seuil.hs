seuilInt :: Int -> Int -> Int -> Int
seuilInt value xmin xmax 
    | value > xmax = xmax 
    | value < xmin = xmin
    | otherwise = value

seuilTuple :: Int -> (Int, Int) -> Int
seuilTuple value (min, max)
    | value < min = min
    | value > max = max
    | otherwise = value

main :: IO ()
main = do
    print $ seuilInt 1 10 45

