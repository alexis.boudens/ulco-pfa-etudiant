data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche

-- estWeekend :: Jour -> Bool
estWeekEnd :: Jour -> Bool
estWeekEnd Samedi = True
estWeekEnd Dimanche = True
estWeekEnd _ = False

-- compterOuvrables :: [Jour] -> Int
compterOuvrables :: [Jour] -> Int
compterOuvrables liste = length (filter (not . estWeekEnd) liste)


main :: IO ()
main = do
    print $ estWeekEnd Samedi
    print $ compterOuvrables [Lundi, Mardi, Samedi]

