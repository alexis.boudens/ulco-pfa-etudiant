
data User = User {
    lastName :: String,
    firstName :: String,
    age :: Int
}

showUser :: User -> String
showUser u = lastName u ++ " " ++ firstName u ++ " " ++ (show (age u))

incAge :: User -> User
incAge u = u {age = age u +1}

main :: IO ()
main = do
    let u = User "Toto" "Roger" 41
    putStrLn $ showUser $ incAge u

