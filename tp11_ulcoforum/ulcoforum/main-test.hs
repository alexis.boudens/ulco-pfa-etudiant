{-# LANGUAGE OverloadedStrings #-}

import Control.Monad (when)
import Database.Selda.Backend (runSeldaT)
import Database.Selda.SQLite (sqliteOpen, seldaClose)
import System.Directory (doesFileExist)

import Forum

dbFilename :: String
dbFilename = "forum.db"

main :: IO ()
main = do

    dbExists <- doesFileExist dbFilename
    conn <- sqliteOpen dbFilename
    when (not dbExists) $ runSeldaT dbInit conn

    putStrLn "\n Forum.dbSelectAllForums"
    runSeldaT Forum.dbSelectAllForums conn >>= mapM_ print

    putStrLn "\n Forum.dbSelectAllForumFromId 1"
    runSeldaT (Forum.dbSelectAllForumFromId 1) conn  >>= mapM_ print

    seldaClose conn