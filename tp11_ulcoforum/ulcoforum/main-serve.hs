{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import Control.Monad.IO.Class
import Database.Selda.SQLite 
import Web.Scotty
import Lucid

import Forum

dbFilename :: String
dbFilename = "forum.db"

main :: IO ()
main = scotty 3000 $ do

    get "/" $ do
        html $ renderText $ homePage      
    get "/alldata" $ do
        forums <- liftIO $ withSQLite dbFilename dbSelectAllForums
        html $ renderText $ mkPage forums
    get "/allthreads" $ do
        forums <- liftIO $ withSQLite dbFilename dbSelectAllForums
        html $ renderText $ mkPage forums     
    get "/allthreads/:thread" $ do
        -- thread <- param 1 TODO: récupérer l'id sur la route & l'envoyer dans dbSelectAllForumFromId
        forums <- liftIO $ withSQLite dbFilename (dbSelectAllForumFromId (1::Int))
        html $ renderText $ forumDetails forums        

homePage :: Html ()
homePage = do
    doctypehtml_ $ do
        head_ $ do
            meta_ [charset_ "UTF-8"]
        body_ $ do
            h1_ (a_ [href_ "/"] "Ulcoform")
            a_ [href_ "alldata"] "alldata" 
            " - "
            a_ [href_ "allthreads"] "allthreads" 
            p_ "This is a ulcoforum"
            

mkPage :: [Forum] -> Html ()
mkPage forums = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "forums"
        body_ $ do
            h1_ (a_ [href_ "/"] "Ulcoform")
            h1_ "All data:"
            ul_ $ forM_ forums $ \m -> li_ $ do
                toHtml $ forum_title m
                " ("
                toHtml $ show $ forum_description m
                ")"

forumDetails :: [Forum] -> Html ()
forumDetails forums = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "forums"
        body_ $ do
            h1_ (a_ [href_ "/"] "Ulcoform")
            h1_ "Forum details:"
            ul_ $ forM_ forums $ \m -> li_ $ do
                toHtml $ forum_title m
                " ("
                toHtml $ show $ forum_description m
                ")"                