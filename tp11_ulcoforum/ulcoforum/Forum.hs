{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

module Forum where

import Control.Monad.IO.Class (liftIO)
import Database.Selda
import Database.Selda.SQLite

----------------------------------------------------------------------
-- Forum
----------------------------------------------------------------------

data Forum = Forum
  { forum_id :: ID Forum
  , forum_title :: Text
  , forum_description :: Text
  } deriving (Generic, Show)

instance SqlRow Forum

forum_table :: Table Forum
forum_table = table "forum" [#forum_id :- autoPrimary]

----------------------------------------------------------------------
-- Person
----------------------------------------------------------------------

data Person = Person
  { person_id :: ID Person
  , person_name :: Text
  } deriving (Generic, Show)

instance SqlRow Person

person_table :: Table Person
person_table = table "person" [#person_id :- autoPrimary]


----------------------------------------------------------------------
-- queries
----------------------------------------------------------------------

dbInit :: SeldaT SQLite IO ()
dbInit = do

    createTable forum_table
    tryInsert forum_table
        [ Forum def "Vacances 2020-2021" "Pas de vacances cette année"
        , Forum def "Master info nouveau programme" "Youpie" 
        ]
        >>= liftIO . print

    createTable person_table
    tryInsert person_table
        [ Person def "Orson Welles"
        , Person def "Charlie Chaplin" 
        , Person def "Albert Dupontel"
        , Person def "Claude Perron"
        , Person def "Alfred Abel"
        , Person def "Fritz Lang"]
        >>= liftIO . print

----------------------------------------------------------------------
-- queries
----------------------------------------------------------------------
dbSelectAllForums :: SeldaT SQLite IO [Forum]
dbSelectAllForums = query $ select forum_table

dbSelectAllForumFromId :: Int -> SeldaT SQLite IO [Forum]
dbSelectAllForumFromId id_forum = query $ do
    forums <- select forum_table
    restrict (forums ! #forum_id .== literal (toId id_forum))
    return forums