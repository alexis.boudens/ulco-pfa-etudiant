{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import GHC.Generics
import Data.Aeson
import qualified Data.Yaml as Y

import qualified Data.Text as T

data Address = Address
    { number    :: Int
    , road     :: T.Text
    , zipCode    :: Int
    , city       :: T.Text
    } deriving (Generic, Show)

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    , address      :: Address 
    } deriving (Generic, Show)

instance ToJSON Address
instance ToJSON Person


persons :: [Person]
persons =
    [ Person "John" "Doe" 1970 (Address 42 "Rue du pont" 45330 "Spycker")
    , Person "Haskell" "Curry" 1900 (Address 2 "Rue du soleil" 45330 "Bergues")
    ]

main :: IO ()
main = do
    encodeFile "out3.json" persons
    Y.encodeFile "out3.yaml" persons
