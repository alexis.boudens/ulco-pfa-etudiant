{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import Lucid
import Data.Text.Lazy.IO as T

main :: IO ()
main = do
    T.putStrLn $ renderText monHtml

monHtml :: Html ()
monHtml = do
    h1_ "Hello"
    a_ [href_ "www.google.fr"] "Cliquez ici"
    forM_ [1..4:: Int] $ \ i -> do
        p_ $ toHtml $ show i        