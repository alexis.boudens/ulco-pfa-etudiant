{-# LANGUAGE OverloadedStrings #-}
import Data.Aeson

import qualified Data.Text as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

instance ToJSON Person where toJSON p = object [ "first" .= firstname p, "birth" .= birthyear p, "last" .= lastname p]

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970
    , Person "Haskell" "Curry" 1900
    ]

main :: IO ()
main = encodeFile "out2.json" persons
