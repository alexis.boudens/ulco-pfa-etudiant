{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

import Database.Selda
import Database.Selda.SQLite
import GHC.Generics (Generic)

data Artist = Artist 
    { artist_id   :: ID Artist
    , artist_name :: Text
    } deriving (Generic, Show)

instance SqlRow Artist

data Title = Title 
    { title_id   :: ID Title
    , title_artist :: ID Artist
    , title_name :: Text
    } deriving (Generic, Show)

instance SqlRow Title

artist_table :: Table Artist
artist_table = table "artist" [#artist_id :- autoPrimary]

title_table :: Table Title
title_table = table "title" 
    [ #title_id :- autoPrimary
    , #title_artist :- foreignKey artist_table #artist_id ]

selectAllArtists :: SeldaT SQLite IO [Artist]
selectAllArtists = query $ select artist_table

selectAllData :: SeldaT SQLite IO [Text :*: Text]
selectAllData = query $ do
    titles <- select title_table
    artists <- select artist_table
    restrict (titles ! #title_artist .== artists ! #artist_id)
    return (artists ! #artist_name :*: titles ! #title_name)

main :: IO ()
main = withSQLite "music.db" selectAllData >>= mapM_ print