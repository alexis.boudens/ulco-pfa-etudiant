{-# LANGUAGE OverloadedStrings #-}

import Control.Monad (when)
import Database.Selda.Backend (runSeldaT)
import Database.Selda.SQLite (sqliteOpen, seldaClose)
import System.Directory (doesFileExist)

import Movie

dbFilename :: String
dbFilename = "movie.db"

main :: IO ()
main = do

    dbExists <- doesFileExist dbFilename
    conn <- sqliteOpen dbFilename
    when (not dbExists) $ runSeldaT dbInit conn

    putStrLn "\nMovie.dbSelectAllMovies"
    runSeldaT Movie.dbSelectAllMovies conn >>= mapM_ print

    putStrLn "\nMovie.dbSelectAllMoviesFromId 1"
    runSeldaT (Movie.dbSelectAllMoviesFromId 1) conn  >>= mapM_ print
    
    {-
    putStrLn "\nMovie.dbSelectMovieFromPersonId 1"
    withSQLite Movie.dbSelectMovieFromPersonId conn (1::Int) >>= mapM_ print
    -}
    seldaClose conn